package main;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Main extends JFrame {
	
	BuildPanel bp;
	int width = 0;
	int height = 0;
	public Main() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		width = (int)screenSize.getWidth();
		height = (int)screenSize.getHeight();
		setBounds(0,0,width,height);
		bp = new BuildPanel(this);
		setLayout(new BorderLayout());
		add(bp, BorderLayout.CENTER);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Main run = new Main();
		run.setVisible(true);
		
	}

}
class BuildPanel extends JPanel implements MouseListener, ActionListener,MouseMotionListener,KeyListener {
	Main main;
	JButton loadImage = new JButton("  Load Image ");
	JButton loadTileset = new JButton(" Load Tileset ");
	JButton changeMapSize = new JButton(" Start Map ");
	JButton saveArea = new JButton(" Save ");
	JTextField mapX = new JTextField();
	JTextField mapY = new JTextField();
	
	Graphics2D draw;
	
	
	//BufferedImage[] tileset = new BufferedImage[21];
	ArrayList<BufferedImage> tileset = new ArrayList<BufferedImage>();
	ArrayList<Rectangle2D> tilesetContact = new ArrayList<Rectangle2D>();
	ArrayList<BufferedImage> tileset16 = new ArrayList<BufferedImage>();
	ArrayList<Rectangle2D> tileset16Contact = new ArrayList<Rectangle2D>();
	ArrayList<String> tilesetNames = new ArrayList<String>();
	ArrayList<ArrayList<BufferedImage>> map = new ArrayList<ArrayList<BufferedImage>>();
	ArrayList<ArrayList<Integer>> mapIndices = new ArrayList<ArrayList<Integer>>();
	ArrayList<ArrayList<BufferedImage>> map16 = new ArrayList<ArrayList<BufferedImage>>();
	ArrayList<ArrayList<Integer>> mapIndices16 = new ArrayList<ArrayList<Integer>>();
	
	BufferedImage screen;
	
	int selectedTile = 0;
	
	int mapSizeX = 0;
	int mapSizeY = 0;
	
	Point offset = new Point(0,0);
	
	String lastDirectory="";
	
	BuildPanel(Main main) {
		this.main = main;
		
		screen = new BufferedImage(main.width,main.height, BufferedImage.TYPE_INT_ARGB);
		
		setBackground(Color.black);
		setForeground(Color.black);
		setLayout(null);
		loadImage.setBounds(10, 10, 120,30);
		loadImage.addActionListener(this);
		
		loadTileset.setBounds(10,50,120,30);
		loadTileset.addActionListener(this);
		
		changeMapSize.setBounds(140,10,120,30);
		changeMapSize.addActionListener(this);
		
		saveArea.setBounds(270,10,120,30);
		saveArea.addActionListener(this);
		
		mapX.setBounds(790,10,100,30);
		
		mapY.setBounds(900,10,100,30);
		
		add(loadImage);
		add(loadTileset);
		add(mapX);
		add(mapY);
		add(changeMapSize);
		add(saveArea);
		
		setFocusable(true);
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
	}
	
	public void paintComponent(Graphics g) {
		Graphics2D g2D = (Graphics2D)g;
		g2D.setColor(Color.black);
		
		Graphics2D draw = (Graphics2D)screen.getGraphics();
		draw.setColor(Color.black);
		draw.fill(new Rectangle2D.Float(0,0,screen.getWidth(), screen.getHeight()));
		
		draw.setColor(Color.blue);
		
		
		for (int i = 0; i < tileset.size();i++) {
			int y = 90 + i*32;
			int x = 10;		
			if (i == selectedTile) {
				draw.fillRect(x-5, y-5, 40, 40);
			}
			
			draw.drawImage(tileset.get(i), x,y,null);
		}
		for (int i = 0; i < tileset16.size();i++) {
			int y = 90 + i*16;
			int x = 50;		
			if (i == selectedTile-tileset.size()) {
				draw.fillRect(x-5, y-5,20, 20);
			}
			
			draw.drawImage(tileset16.get(i), x,y,null);
		}
		
		int posY = 50+offset.y;
		for (int y = 0; y < mapSizeY; y++) {
			int posX = 150+offset.x;
			
			for (int x = 0; x < mapSizeX;x++) {
				
				draw.drawImage(map.get(y).get(x),posX,posY,null);
				posX += 32;
			}
			posY += 32;
		}
		posY = 50+offset.y;
		for (int y = 0; y < mapSizeY*2; y++) {
			int posX = 150+offset.x;
			
			for (int x = 0; x < mapSizeX*2;x++) {
				
				draw.drawImage(map16.get(y).get(x),posX,posY,null);
				posX +=16;
			}
			posY += 16;
		}
		
		
		BasicStroke type = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
	    draw.setStroke(type);
		
		int x =150+offset.x;
		int y = 50+offset.y;
		draw.setColor(Color.white);
		for (int i = 0; i <=mapSizeX;i++) {
			
			draw.draw(new Line2D.Double(x+i*32, y, x+i*32, y+mapSizeY*32));
		}
		x = 150+offset.x;
		for (int i = 0; i <= mapSizeY; i++) {
			
			draw.draw(new Line2D.Double(x, y+i*32, x+mapSizeX*32, y+i*32));
		}
		BasicStroke type2 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
	    draw.setStroke(type2);
		x =150+offset.x;
		y = 50+offset.y;
		draw.setColor(Color.white);
		for (int i = 0; i <=mapSizeX*2;i++) {
			
			draw.draw(new Line2D.Double(x+i*16, y, x+i*16, y+mapSizeY*32));
		}
		x = 150+offset.x;
		for (int i = 0; i <= mapSizeY*2; i++) {
			
			draw.draw(new Line2D.Double(x, y+i*16, x+mapSizeX*32, y+i*16));
		}
		g2D.drawImage(screen, 0,0, null);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		for (int i = 0; i < tilesetContact.size();i++) {
			Rectangle2D r = tilesetContact.get(i);
			if (r.contains(e.getX(), e.getY())) {
				selectedTile = i;
			}
		}
		for (int i = 0; i < tileset16Contact.size();i++) {
			Rectangle2D r = tileset16Contact.get(i);
			if (r.contains(e.getX(), e.getY())) {
				selectedTile = i+tileset.size();
			}
		}
		if (selectedTile < tileset.size()) {
			int posY = 50+offset.y;
			for (int y = 0; y < mapSizeY; y++) {
				int posX = 150+offset.x;
			
				for (int x = 0; x < mapSizeX;x++) {
				
					Rectangle2D.Float rec = new Rectangle2D.Float(posX,posY,32,32);
					//System.out.println(rec.toString()+" " +e.getPoint().toString());
					if (rec.contains(e.getPoint())) {
						//System.out.println("Contact");
						ArrayList<BufferedImage> row = map.get(y);
						ArrayList<Integer> rowIndices = mapIndices.get(y);
						//System.out.println(y+"");
						switch(e.getModifiers()) {
					      case InputEvent.BUTTON1_MASK: {
					    	  row.set(x, tileset.get(selectedTile));
								rowIndices.set(x, selectedTile); 
					        break;
					        }
					      case InputEvent.BUTTON2_MASK: {   
					        break;
					        }
					      case InputEvent.BUTTON3_MASK: {
					    	  row.set(x, new  BufferedImage(32,32, BufferedImage.TYPE_INT_ARGB));
								rowIndices.set(x,-1);  
					        break;
					        }
					      }
						
						
					}
					posX += 32;
				}
				posY += 32;
			}
		} else if (selectedTile >= tileset.size()) {
			
			int posY = 50+offset.y;
			for (int y = 0; y < mapSizeY*2; y++) {
				int posX = 150+offset.x;
				
				for (int x = 0; x < mapSizeX*2;x++) {
					
					Rectangle2D.Float rec = new Rectangle2D.Float(posX,posY,16,16);
					//System.out.println(rec.toString()+" " +e.getPoint().toString());
					if (rec.contains(e.getPoint())) {
						//System.out.println("Contact");
						ArrayList<BufferedImage> row = map16.get(y);
						ArrayList<Integer> rowIndices = mapIndices16.get(y);
						//System.out.println(y+"");
						switch(e.getModifiers()) {
					      case InputEvent.BUTTON1_MASK: {

								row.set(x, tileset16.get(selectedTile-tileset.size()));
								rowIndices.set(x, selectedTile-tileset.size());
								break;
					        }
					      case InputEvent.BUTTON2_MASK: {   
					        break;
					        }
					      case InputEvent.BUTTON3_MASK: {
					    	  row.set(x, new  BufferedImage(16,16, BufferedImage.TYPE_INT_ARGB));
								rowIndices.set(x,-1);  
					        break;
					        }
					      }
					}
					posX += 16;
				}
				posY += 16;
			}
		}
		repaint();
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if (e.getSource() == loadImage) {
			JFileChooser fc = new JFileChooser();
			if (lastDirectory != "")
				fc.setCurrentDirectory(new File(lastDirectory));
			int returnVal =fc.showOpenDialog(this);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File file = fc.getSelectedFile();
	            lastDirectory = file.getAbsolutePath();
	            tilesetNames.add(file.getAbsolutePath());
	            BufferedImage img = null;
	            try {
	                img = ImageIO.read(file);
	            } catch (IOException ex) {
	            }
	            if (img.getHeight() == 16) {
	            	tileset16.add(img);
		            tileset16Contact.add(new Rectangle2D.Float(50,90+(tileset16.size()-1)*16,16,16));
	            } else if (img.getHeight() == 32) {
	            	tileset.add(img);
		            tilesetContact.add(new Rectangle2D.Float(10,90+(tileset.size()-1)*32,32,32));
	            }
	            
	            repaint();
	         } else {
	         }
		} else if (e.getSource() == loadTileset) {
			JFileChooser fc = new JFileChooser();
			if (lastDirectory != "")
				fc.setCurrentDirectory(new File(lastDirectory));
			int returnVal =fc.showOpenDialog(this);
	
			String sizeS = (String)JOptionPane.showInputDialog(this,  "Tile Size?:", "Filename",  JOptionPane.PLAIN_MESSAGE,  null,  null,
	                null);
			
			String dimensionsS = (String)JOptionPane.showInputDialog(this,  "Enter the number of tiles::", "Filename",  JOptionPane.PLAIN_MESSAGE,  null,  null,
	                null);
			int size;
			int dim;
			try {
				size = Integer.parseInt(sizeS);
				dim = Integer.parseInt(dimensionsS);
			} catch (Exception ex) {
				return;
			}
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File file = fc.getSelectedFile();
	            lastDirectory = file.getAbsolutePath();
	            tilesetNames.add(file.getAbsolutePath());
	            BufferedImage img = null;
	            try {
	                img = ImageIO.read(file);
	            } catch (IOException ex) {
	            }
	            if (size == 32) {
	            	tileset.clear();
	            	tilesetContact.clear();
	            } else if (size == 16) {
	            	tileset16.clear();
	            	tileset16Contact.clear();
	            }
	            for (int i = 0; i < dim; i++) {
	            	int x = size*(i%5);
	            	int y = size*(i/5);
	            	BufferedImage snap = new BufferedImage(size,size,BufferedImage.TYPE_INT_ARGB);
	            	Graphics2D g2d = snap.createGraphics();
	            	g2d.drawImage(img, -x, -y, null);
	            	if (size == 32)  {
		            	tileset.add(snap);
		            	 tilesetContact.add(new Rectangle2D.Float(10,90+(i)*size,size,size));
	            	}else if (size == 16){
		            	tileset16.add(snap);
		            	 tileset16Contact.add(new Rectangle2D.Float(50,90+(i)*size,size,size));
	            	}
	            	g2d.dispose();
	            }
	            
	            repaint();
			}
		} else if (e.getSource() == changeMapSize) {
			int newMapSizeX = 0;
			int newMapSizeY = 0;
			
			try {
				newMapSizeX = Integer.parseInt(mapX.getText());
				newMapSizeY = Integer.parseInt(mapY.getText());
				
			} catch (Exception ex) {
				
			}
			initMap(map,mapIndices,newMapSizeX,newMapSizeY,1);
			initMap(map16,mapIndices16,newMapSizeX,newMapSizeY,2);
			mapSizeY = newMapSizeY;
			mapSizeX = newMapSizeX;
			repaint();
		} else if (e.getSource() == saveArea) {
			saveArea();
		}
	}
	
	public void saveArea() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Specify a folder to save in:");   
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int userSelection = fileChooser.showSaveDialog(this);
		 File folder = null;
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    folder = fileChooser.getSelectedFile();
		} else {
			return;
		}
		
		String name = (String)JOptionPane.showInputDialog(this,  "Enter a name:", "Filename",  JOptionPane.PLAIN_MESSAGE,  null,  null,
                null);
		if (name.equals(""))
			return;
		PrintWriter out = null;
		try {
			out = new PrintWriter(folder.getAbsolutePath()+"/"+name+".area");
			out.println("\'tiledictbase="+name+"base");
			out.println("\'tiledictmid="+name+"mid");
			out.println(":dimensions");
			out.println("x="+mapSizeX);
			out.println("y="+mapSizeY);
			out.println(":base");
			for (int y = 0; y < map.size();y++) {
				ArrayList<Integer> row = mapIndices.get(y);
				String line = "";
				for (int x = 0; x < row.size(); x++) {
					line += " " + row.get(x);
				}
				out.println(line);
			}
			out.println(":mid");
			for (int y = 0; y < map16.size();y++) {
				ArrayList<Integer> row = mapIndices16.get(y);
				String line = "";
				for (int x = 0; x < row.size(); x++) {
					line += " " + row.get(x);
				}
				out.println(line);
			}
			
			BufferedImage baseDict = new BufferedImage(32*5,32*(tileset.size()/5+1), BufferedImage.TYPE_INT_ARGB);
			Graphics2D draw = (Graphics2D)baseDict.getGraphics();
			draw.setColor(Color.black);
			for (int i = 0; i < tileset.size(); i++) {
				draw.drawImage(tileset.get(i),32*(i%5),32*(i/5) ,null);
			}
			File outputfile = new File(folder.getAbsolutePath()+"/"+name+"base.png");
			ImageIO.write(baseDict, "png", outputfile);
			
			BufferedImage midDict = new BufferedImage(16*5,16*(tileset.size()/5+1), BufferedImage.TYPE_INT_ARGB);
			draw = (Graphics2D)midDict.getGraphics();
			draw.setColor(Color.black);
			for (int i = 0; i < tileset16.size(); i++) {
				draw.drawImage(tileset16.get(i),16*(i%5),16*(i/5) ,null);
			}
			outputfile = new File(folder.getAbsolutePath()+"/"+name+"mid.png");
			ImageIO.write(midDict, "png", outputfile);
			
			
		} catch(IOException e) {
			System.out.println(e.getMessage());
		} finally {
			out.close();
		}
		
		
	}
	public void initMap(ArrayList<ArrayList<BufferedImage>> map, ArrayList<ArrayList<Integer>> mapIndices, int newMapSizeX, int newMapSizeY,int multiple) {
		if (newMapSizeY > mapSizeY) {
			for (int i = mapSizeY*multiple; i < newMapSizeY*multiple; i++) {
				map.add(new ArrayList<BufferedImage>());
				mapIndices.add(new ArrayList<Integer>());
			}
		} else if (newMapSizeY < mapSizeY) {
			for (int i = mapSizeY*multiple; i > newMapSizeY*multiple; i--) {
				map.remove(map.size()-1);
				mapIndices.remove(mapIndices.size()-1);
			}
		}
		int k = 0;
		for (int i = 0; i < map.size(); i++) {
			
			ArrayList<BufferedImage> row = map.get(i);
			ArrayList<Integer> rowIndices = mapIndices.get(i);
			if (newMapSizeX*multiple > row.size()){
				for (int j = row.size(); j < newMapSizeX*multiple; j++) {
					row.add(new BufferedImage(32,32, BufferedImage.TYPE_INT_ARGB));
					rowIndices.add(-1);
				}
			} else if (newMapSizeX*multiple < row.size()) {
				for (int j = row.size(); j > newMapSizeX*multiple; j--) {
					row.remove(row.size()-1);
					rowIndices.remove(-1);					
				}
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		
		if (selectedTile < tileset.size()) {
			int posY = 50+offset.y;
			for (int y = 0; y < mapSizeY; y++) {
				int posX = 150+offset.x;
			
				for (int x = 0; x < mapSizeX;x++) {
				
					Rectangle2D.Float rec = new Rectangle2D.Float(posX,posY,32,32);
					//System.out.println(rec.toString()+" " +e.getPoint().toString());
					if (rec.contains(e.getPoint())) {
						//System.out.println("Contact");
						ArrayList<BufferedImage> row = map.get(y);
						ArrayList<Integer> rowIndices = mapIndices.get(y);
						//System.out.println(y+"");
						switch(e.getModifiers()) {
					      case InputEvent.BUTTON1_MASK: {
					    	  row.set(x, tileset.get(selectedTile));
								rowIndices.set(x, selectedTile); 
					        break;
					        }
					      case InputEvent.BUTTON2_MASK: {   
					        break;
					        }
					      case InputEvent.BUTTON3_MASK: {
					    	  row.set(x, new  BufferedImage(32,32, BufferedImage.TYPE_INT_ARGB));
								rowIndices.set(x,-1);  
					        break;
					        }
					      }
						
						
					}
					posX += 32;
				}
				posY += 32;
			}
		} else if (selectedTile >= tileset.size()) {
				
			int posY = 50;
			for (int y = 0; y < mapSizeY*2; y++) {
				int posX = 150;
				
				for (int x = 0; x < mapSizeX*2;x++) {
					
					Rectangle2D.Float rec = new Rectangle2D.Float(posX,posY,16,16);
					//System.out.println(rec.toString()+" " +e.getPoint().toString());
					if (rec.contains(e.getPoint())) {
						//System.out.println("Contact");
						ArrayList<BufferedImage> row = map16.get(y);
						ArrayList<Integer> rowIndices = mapIndices16.get(y);
						//System.out.println(y+"");
						switch(e.getModifiers()) {
					      case InputEvent.BUTTON1_MASK: {

								row.set(x, tileset16.get(selectedTile-tileset.size()));
								rowIndices.set(x, selectedTile-tileset.size());
								break;
					        }
					      case InputEvent.BUTTON2_MASK: {   
					        break;
					        }
					      case InputEvent.BUTTON3_MASK: {
					    	  row.set(x, new  BufferedImage(16,16, BufferedImage.TYPE_INT_ARGB));
								rowIndices.set(x,-1);  
					        break;
					        }
					      }
					}
					posX += 16;
				}
				posY += 16;
			}
		}
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		requestFocusInWindow();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int c = e.getKeyCode ();
        int rate = 20;
		if (c==KeyEvent.VK_UP) {                
            offset.y -= rate;
        } else if(c==KeyEvent.VK_DOWN) {                
           offset.y += rate;
        } else if(c==KeyEvent.VK_LEFT) {                
            offset.x -= rate; 
        } else if(c==KeyEvent.VK_RIGHT) {                
            offset.x += rate;   
        }
		System.out.println(offset.toString());
        repaint();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}